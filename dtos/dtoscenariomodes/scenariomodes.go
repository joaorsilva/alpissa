package dtoscenariomodes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

// Dto - ScenarioMode Dto
type Dto struct {
	ID          int    `json:"id"`
	DisplayName string `json:"displayName"`
	GameName    string `json:"gameName"`
	Decription  string `json:"description"`
}

// Dtos - List of ScenarioModes Dto
type Dtos []*Dto

const (
	// Dir - ScenarioModes directory
	Dir = "config"
	// File - ScenarioModes file name
	File = "ScenarioModes.json"
)

var (
	//Data - ScenarioModes data
	Data  Dtos
	maxID int
)

// Load - Loads the file data.
func Load() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "ScenarioModes", "Loading ScenarioModes configuration")

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return fmt.Errorf("file '%s' doesn't exist", path)
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	return nil
}

// Save - Saves a Scenario Mode object
func (scenariomode *Dto) Save() *httperrors.HTTPError {

	// Validates the incomming data
	if errValidate := scenariomode.Validate(); errValidate != nil {
		return errValidate
	}

	if scenariomode.ID == 0 {
		// New object
		// Increments the ID
		maxID++
		// Assigns the new ID to the new object
		scenariomode.ID = maxID
		// Inserts the new object into the Data array
		Data = append(Data, scenariomode)
	} else {
		// Existing object
		var found bool = false
		// Loop through the array
		for index, data := range Data {
			// Saerches for the existing object by its ID
			if scenariomode.ID == data.ID {
				// Assigns the new data to the object position in the array
				Data[index] = scenariomode
				// Flags that the object was found
				found = true
				// Breaks the loop
				break
			}
		}

		// If the object was not found
		if found == false {
			// Return not found error
			return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
		}
	}

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Delete - Deletes a Scenario Mode object
func (scenariomode *Dto) Delete() *httperrors.HTTPError {

	// Finds the object in the array by its ID
	var pos int = -1
	for index, data := range Data {
		if scenariomode.ID == data.ID {
			pos = index
			break
		}
	}

	if pos == -1 {
		// If the ID wasn't found return a not found error
		return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
	}

	// Removes the object from the array shifting it's position so it will not be empty
	Data[pos] = Data[len(Data)-1]
	Data[len(Data)-1] = nil
	Data = Data[:len(Data)-1]

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Validate - Validates a Scenario Mode object
func (scenariomode *Dto) Validate() *httperrors.HTTPError {

	// Trim any leftover spaces
	scenariomode.DisplayName = strings.TrimSpace(scenariomode.DisplayName)

	// Checks if the required field value is empty
	if scenariomode.DisplayName == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "displayName", Message: "the field displayName is required"}
	}

	// Trim any leftover spaces
	scenariomode.GameName = strings.TrimSpace(scenariomode.GameName)

	// Checks if the required field value is empty
	if scenariomode.GameName == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "gameName", Message: "the field GameName is required"}
	}

	// Trim any leftover spaces
	scenariomode.Decription = strings.TrimSpace(scenariomode.Decription)

	return nil
}

func save() *httperrors.HTTPError {

	// Marshal the array data into JSON
	jsonString, errMarshal := json.MarshalIndent(Data, "", "    ")
	if errMarshal != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: "failed to marshal to JSON"}
	}

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("file '%s' doesn't exist", path)}
	}

	// Writes the file
	if errWrite := ioutil.WriteFile(path, jsonString, 0644); errWrite != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("failed to save file %s. ERROR: %s", path, errWrite.Error())}
	}

	return nil
}
