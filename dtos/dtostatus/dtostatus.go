package dtostatus

// SteamCMDDto - Dto for SteamCMD status data
type SteamCMDDto struct {
	IsDownloaded  bool   `json:"isDownloaded"`
	IsDownloading bool   `json:"isDownloading"`
	IsInstaled    bool   `json:"isInstaled"`
	IsInstaling   bool   `json:"isInstaling"`
	IsRemoving    bool   `json:"isRemoving"`
	Version       string `json:"version"`
}

// SandstormDto - Dto for Sandstorm status data
type SandstormDto struct {
	IsInstaled  bool   `json:"isInstaled"`
	IsInstaling bool   `json:"isInstaling"`
	Version     string `json:"version"`
}

// Dto - Dto structure for server status
type Dto struct {
	Steam             SteamCMDDto  `json:"steam"`
	Sandstorm         SandstormDto `json:"sandstorm"`
	NumConfigurations int          `json:"numConfigurations"`
	NumRunningServers int          `json:"numRunningServers"`
}

var (
	// Data - Data for server status
	Data Dto
)
