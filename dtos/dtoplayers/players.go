package dtoplayers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/utils"
	steamAPI "github.com/ljesus/steam-go"
)

// SteamDto - Dto for Steam User
type SteamDto struct {
	SetamID         string `json:"steamid"`
	PersonaName     string `json:"personaname"`
	ProfileURL      string `json:"profileurl"`
	AvatarURL       string `json:"avatar"`
	AvatarMediumURL string `json:"avatarmedium"`
	AvatarLargeURL  string `json:"avatarfull"`
	VisibilityState int    `json:"communityvisibilitystate"`
}

// Dto - Players Dto
type Dto struct {
	ID          int       `json:"id"`
	Steam       SteamDto  `json:"steam"`
	LastIP      string    `json:"lastIP"`
	PlayedTime  time.Time `json:"playedTime"`
	MaxScore    int       `json:"maxScore"`
	TotalScore  int       `json:"totalScore"`
	PlayedTimes int       `json:"playedTimes"`
}

// Dtos - List of Players Dto
type Dtos []*Dto

const (
	// Dir - Players directory
	Dir = "config"
	// File - Players file name
	File = "Players.json"
)

var (
	//Data - Players data
	Data  Dtos
	maxID int
)

// Load - Loads the file data.
func Load() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Players", "Loading Players configuration")

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return fmt.Errorf("file '%s' doesn't exist", path)
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	return nil
}

// Save - Saves a Player object
func (player *Dto) Save() *httperrors.HTTPError {

	// Validates the incomming data
	if errValidate := player.Validate(); errValidate != nil {
		return errValidate
	}

	if player.ID == 0 {
		// New object
		// Increments the ID
		maxID++
		// Assigns the new ID to the new object
		player.ID = maxID
		// Inserts the new object into the Data array
		Data = append(Data, player)
	} else {
		// Existing object
		var found bool = false
		// Loop through the array
		for index, data := range Data {
			// Saerches for the existing object by its ID
			if player.ID == data.ID {
				// Assigns the new data to the object position in the array
				Data[index] = player
				// Flags that the object was found
				found = true
				// Breaks the loop
				break
			}
		}

		// If the object was not found
		if found == false {
			// Return not found error
			return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
		}
	}

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Delete - Deletes a Player object
func (player *Dto) Delete() *httperrors.HTTPError {

	// Finds the object in the array by its ID
	var pos int = -1
	for index, data := range Data {
		if player.ID == data.ID {
			pos = index
			break
		}
	}

	if pos == -1 {
		// If the ID wasn't found return a not found error
		return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
	}

	// Removes the object from the array shifting it's position so it will not be empty
	Data[pos] = Data[len(Data)-1]
	Data[len(Data)-1] = nil
	Data = Data[:len(Data)-1]

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Validate - Validates a Player object
func (player *Dto) Validate() *httperrors.HTTPError {

	// Validates the Steam sub-document
	if errSteam := player.Steam.ValidateSteam(); errSteam != nil {
		return errSteam
	}

	// Trim any leftover spaces
	player.Steam.SetamID = strings.TrimSpace(player.Steam.SetamID)

	// Checks if the required field value is empty
	if player.Steam.SetamID == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "displayName", Message: "the field displayName is required"}
	}

	return nil
}

// ValidateSteam - Validates the Steam sub-document
func (steam *SteamDto) ValidateSteam() *httperrors.HTTPError {

	steam.SetamID = strings.TrimSpace(steam.SetamID)
	if steam.SetamID == "" {
		return &httperrors.HTTPError{Code: 400, Context: "gameName", Message: "the field steam.setamid is required"}
	}

	steam.PersonaName = strings.TrimSpace(steam.PersonaName)
	steam.ProfileURL = strings.TrimSpace(steam.ProfileURL)
	steam.AvatarURL = strings.TrimSpace(steam.AvatarURL)
	steam.AvatarMediumURL = strings.TrimSpace(steam.AvatarMediumURL)
	steam.AvatarLargeURL = strings.TrimSpace(steam.AvatarLargeURL)

	if steam.PersonaName == "" ||
		steam.ProfileURL == "" ||
		steam.AvatarURL == "" ||
		steam.AvatarMediumURL == "" ||
		steam.AvatarLargeURL == "" {

		api := steamAPI.NewSteamApi(dtoconfig.Data.SteamAPYKey)

		applog.AppLogs["ALPISSA"].Log("INFO", "Players", fmt.Sprintf("Contacting Steam for player ID: %s data.", steam.SetamID))

		data, errSteam := api.GetPlayerSummaries([]string{steam.SetamID})
		if errSteam != nil {
			return &httperrors.HTTPError{Code: 400, Context: "document", Message: fmt.Sprintf("the steam API failed to return. ERROR: %s", errSteam.Error())}
		}

		steam.PersonaName = data.Response.Players[0].PersonaName
		steam.ProfileURL = data.Response.Players[0].ProfileUrl
		steam.AvatarURL = data.Response.Players[0].Avatar
		steam.AvatarMediumURL = data.Response.Players[0].AvatarMedium
		steam.AvatarLargeURL = data.Response.Players[0].AvatarFull
	}

	return nil

}

func save() *httperrors.HTTPError {

	// Marshal the array data into JSON
	jsonString, errMarshal := json.MarshalIndent(Data, "", "    ")
	if errMarshal != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: "failed to marshal to JSON"}
	}

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("file '%s' doesn't exist", path)}
	}

	// Writes the file
	if errWrite := ioutil.WriteFile(path, jsonString, 0644); errWrite != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("failed to save file %s. ERROR: %s", path, errWrite.Error())}
	}

	return nil
}
