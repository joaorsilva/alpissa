package dtorunners

import (
	"fmt"
	"time"

	"bitbucket.org/joaorsilva/alpissa/services/applog"

	"github.com/riking/homeapi/rcon"
)

const (
	RconTimeout = 30
)

// RconConnect - Connects to RCon on localhost
func RconConnect(running *Dto) (*rcon.Client, error) {

	applog.AppLogs[running.Server.DisplayName].Log("INFO", "RCon", fmt.Sprintf("Connection RCon client to %s:%d", "127.0.0.1", running.Server.RCon.Port))
	client, err := rcon.DialTimeout(running.Server.RCon.Bind, running.Server.RCon.Port, running.Server.RCon.Password, RconTimeout*time.Second)
	if err != nil {
		applog.AppLogs[running.Server.DisplayName].Log("WARNING", "RCon", "RCon client failed to connect (will retry)")
		return nil, err
	}
	applog.AppLogs[running.Server.DisplayName].Log("INFO", "RCon", "RCon client connected")
	return &client, nil
}

// RconCommand - Send a RCon command
func RconCommand(rc *rcon.Client, command string) (string, error) {
	return rc.Command(command)
}

// RconDisconenct - Disconnects from RCon server
func RconDisconenct(running *Dto, rc *rcon.Client) error {
	applog.AppLogs[running.Server.DisplayName].Log("INFO", "RCon", "RCon disconnected")
	return rc.Close()
}
