package dtorunners

import (
	"fmt"
	"math"
	"os"
	"os/exec"
	"strings"
	"time"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/utils/ini"
	"github.com/riking/homeapi/rcon"
	"github.com/rumblefrog/go-a2s"
	"github.com/shirou/gopsutil/process"
)

// Launch - Lunches a new ruuner server
func Launch(runner *Dto, critical *int) {

	// Starting log service for runner
	errLog := applog.New(runner.Server.DisplayName, fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.LogPath))
	if errLog != nil {
		runner.ExpectedToRun = false
		runner.Status = StatusStopped
		*critical = -1
		applog.AppLogs["ALPISSA"].Log("ERROR", "Launcher", fmt.Sprintf("Server '%s' couldn't create log. Terminating.", runner.Server.DisplayName))
		return
	}

	// Switch the configuration files
	if errConfig := switchConfig(runner); errConfig != nil {
		runner.ExpectedToRun = false
		runner.Status = StatusStopped
		*critical = -1
		applog.AppLogs["ALPISSA"].Log("ERROR", "Launcher", fmt.Sprintf("Server '%s' couldn't create the configuration files. ERROR: %s", runner.Server.DisplayName, errConfig.Error()))
		return
	}

	// Start Sandstorm
	parameters := buildCmdLine(runner)
	strCmd := fmt.Sprintf("%s/%s/sandstorm-server/Insurgency/Binaries/Linux/InsurgencyServer-Linux-Shipping %s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, strings.Join(*parameters, " "))
	applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Laucher", fmt.Sprintf("Launch: %s", strCmd))
	cmd := exec.Command(fmt.Sprintf("%s/%s/sandstorm-server/Insurgency/Binaries/Linux/InsurgencyServer-Linux-Shipping", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath), *parameters...)
	runner.Cmd = cmd
	errRun := cmd.Start()
	if errRun != nil {
		runner.ExpectedToRun = false
		runner.Status = StatusStopped
		*critical = -1
		return
	}

	// RCon connect
	attempts := 0
	var rc *rcon.Client = nil
	var errRCon error
	for rc == nil && attempts < 3 {
		time.Sleep(5 * time.Second)
		rc, errRCon = RconConnect(runner)
		if errRCon != nil {
			attempts++
		}
	}

	if rc == nil {
		applog.AppLogs[runner.Server.DisplayName].Log("ERROR", "RCon", "RCon connection failed")
		KillRunner(runner)
		runner.ExpectedToRun = false
		runner.Status = StatusStopped
		*critical = -1
		return
	}

	// Loop for information
	go func(runner *Dto, rc *rcon.Client) {
		for runner.ExpectedToRun == true {
			rc.Command("listplayers")
			queryServer(runner)
			queryProcess(runner)
			time.Sleep(5 * time.Second)
		}
	}(runner, rc)

	// Leave critical section
	*critical = -1
	applog.AppLogs[runner.Server.DisplayName].Log("INFO", "RCon", "RCon connected")

	runner.Status = StatusRunning

	// Wait for Sandstorm to terminate
	cmd.Wait()

	RconDisconenct(runner, rc)

	runner.Status = StatusStopped
	runner.Cmd = nil
	applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Launcher", "Server terminated.")
	applog.AppLogs["ALPISSA"].Log("INFO", "Launcher", fmt.Sprintf("Server '%s' normal stop. Terminating.", runner.Server.DisplayName))

}

// KillRunner - Kills a server runner
func KillRunner(runner *Dto) {
	if runner.Cmd != nil && runner.Cmd.Process != nil {
		runner.Cmd.Process.Signal(os.Interrupt)
	}
}

func queryProcess(runner *Dto) {
	if runner.Cmd != nil && runner.Cmd.Process != nil {

		proc, errProc := process.NewProcess(int32(runner.Cmd.Process.Pid))
		if errProc != nil {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect process information.")
			return
		}
		percentCPU, errCPU := proc.CPUPercent()
		if errCPU == nil {
			runner.Metrics.CPUPercent = percentCPU
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect CPU process information.")
		}

		percentMemory, errMemory := proc.MemoryPercent()
		if errMemory == nil {
			runner.Metrics.MEMPercent = percentMemory
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect Memory process information.")
		}

		treads, errThreads := proc.NumThreads()
		if errThreads == nil {
			runner.Metrics.NumThreads = treads
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect Threads process information.")
		}

		fds, errFDS := proc.NumFDs()
		if errFDS == nil {
			runner.Metrics.NumFDS = fds
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect File Descriptors process information.")
		}

		isRunning, errRunning := proc.IsRunning()
		if errRunning == nil {
			runner.Metrics.IsRunning = isRunning
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect IsRunning process information.")
		}

		net, errNet := proc.NetIOCounters(false)
		if errNet == nil {
			for _, item := range net {
				runner.Metrics.Net.BytesOut = uint64(math.Round(float64(item.BytesSent) / (1024 * 1024)))
				runner.Metrics.Net.BytesIn = uint64(math.Round(float64(item.BytesRecv) / (1024 * 1024)))
			}
		} else {
			applog.AppLogs[runner.Server.DisplayName].Log("INFO", "Process", "Failed to collect Network process information.")
		}
	}
}

func queryServer(runner *Dto) {

	// Query port data
	client, errQueryConnect := a2s.NewClient(fmt.Sprintf("127.0.0.1:%d", runner.Server.Server.QueryPort))
	if errQueryConnect != nil {
		applog.AppLogs[runner.Server.DisplayName].Log("ERROR", "Query", "Query port connection failed")
	} else {
		info, errQueryInfo := client.QueryInfo()
		if errQueryInfo != nil {
			applog.AppLogs[runner.Server.DisplayName].Log("ERROR", "Query", "Query Info failed")
		} else {
			runner.Settings.Name = info.Name
			runner.Settings.Map = info.Map
			runner.Settings.Players = int(info.Players)
			runner.Settings.Bots = int(info.Bots)
			runner.Settings.VAC = info.VAC
			runner.Settings.Version = info.Version
		}

		rules, errQueryRules := client.QueryRules()
		if errQueryRules != nil {
			applog.AppLogs[runner.Server.DisplayName].Log("ERROR", "Query", "Query Rules failed")
		} else {
			for key, val := range rules.Rules {
				switch key {
				case "CustomRules_b":
					if val == "true" {
						runner.Settings.CustomRules = true
					} else {
						runner.Settings.CustomRules = false
					}
					break
				case "RankedServer_b":
					if val == "true" {
						runner.Settings.Ranked = true
					} else {
						runner.Settings.Ranked = false
					}
					break
				case "Mods_b":
					if val == "true" {
						runner.Settings.Moded = true
					} else {
						runner.Settings.Moded = false
					}
					break
				case "Night_b":
					if val == "true" {
						runner.Settings.Ligting = "Night"
					} else {
						runner.Settings.Ligting = "Day"
					}
					break
				case "Coop_b":
					if val == "true" {
						runner.Settings.Coop = true
					} else {
						runner.Settings.Coop = false
					}
					break
				case "Versus_b":
					if val == "true" {
						runner.Settings.Versus = true
					} else {
						runner.Settings.Versus = false
					}
					break
				case "Mutated_b":
					if val == "true" {
						runner.Settings.Mutated = true
					} else {
						runner.Settings.Mutated = false
					}
					break
				case "OfficialRuleset_b":
					if val == "true" {
						runner.Settings.OfficialRuleset = true
					} else {
						runner.Settings.OfficialRuleset = false
					}
					break
				case "GameMode_s":
					runner.Settings.GameMode = val
					break
				case "ModList_s":
					runner.Settings.ModsList = strings.Split(val, ",")
					break
				case "Mutators_s":
					runner.Settings.Mutators = strings.Split(val, ",")
					break
				}
			}
		}

		client.Close()
	}
}

func switchConfig(runner *Dto) error {

	if err := ini.Writer(&runner.Server.Files.Game, fmt.Sprintf("%s/%s/sandstorm-server/Insurgency/Saved/Config/LinuxServer/Game.ini", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath)); err != nil {
		return err
	}

	if err := ini.Writer(&runner.Server.Files.Engine, fmt.Sprintf("%s/%s/sandstorm-server/Insurgency/Saved/Config/LinuxServer/Engine.ini", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath)); err != nil {
		return err
	}

	return nil
}

func buildCmdLine(runner *Dto) *[]string {

	scenarioParam := fmt.Sprintf("Scenario=%s", runner.Server.Game.DefaultScenario)
	if runner.Server.Game.IsDefaultMapCustom == false {
		scenarioParam = fmt.Sprintf("Scenario=%s_%s_%s?Lighting=%s", runner.Server.Game.DefaultScenario, runner.Server.Game.DefaultMode, runner.Server.Game.DefaultSide, runner.Server.Game.DefaultLight)
	}

	mapParam := fmt.Sprintf("%s?%s?MaxPlayers=%d", runner.Server.Game.DefaultMap, scenarioParam, runner.Server.Server.Players.Visible)
	parameters := []string{}

	parameters = append(parameters, mapParam)
	parameters = append(parameters, fmt.Sprintf("-Port=%d", runner.Server.Server.GamePort))
	parameters = append(parameters, fmt.Sprintf("-QueryPort=%d", runner.Server.Server.QueryPort))
	parameters = append(parameters, fmt.Sprintf("-hostname=%s", runner.Server.Server.Name))
	if runner.Server.Server.Password != "" {
		parameters = append(parameters, fmt.Sprintf("-%s", runner.Server.Server.Password))
	}
	parameters = append(parameters, fmt.Sprintf("-log=Server_%d.log", runner.Server.ID))
	parameters = append(parameters, "-LogCmds=\"LogGameplayEvents Log\"")
	parameters = append(parameters, "-LOCALLOGTIMES")
	parameters = append(parameters, "-Rcon")
	parameters = append(parameters, fmt.Sprintf("-ListenAddressOverride=%s", runner.Server.RCon.Bind))
	parameters = append(parameters, fmt.Sprintf("-RconPassword=%s", runner.Server.RCon.Password))
	parameters = append(parameters, fmt.Sprintf("-RconListenPort=%d", runner.Server.RCon.Port))

	if len(runner.Server.Game.Mutators) > 0 {
		parameters = append(parameters, fmt.Sprintf("-Mutators=%s", strings.Join(runner.Server.Game.Mutators, ",")))
	}

	parameters = append(parameters, "-MapCycle=MapCycle.txt")
	parameters = append(parameters, "-AdminList=Admins.txt")
	parameters = append(parameters, "-ModList=ModList.txt")

	if runner.Server.Server.Gslt != "" {
		parameters = append(parameters, "-GameStats")
		parameters = append(parameters, fmt.Sprintf("-GSLTToken=%s", runner.Server.Server.Gslt))
	}

	return &parameters
}
