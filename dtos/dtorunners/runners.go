package dtorunners

import (
	"fmt"
	"os/exec"
	"time"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoplayers"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
)

// PlayersDto - Dto for playing players
type PlayersDto struct {
	Steam *dtoplayers.SteamDto
	Score uint64
}

// SettingsDto - Dto for server settings
type SettingsDto struct {
	Name            string   `json:"name"`
	Map             string   `json:"map"`
	Players         int      `json:"players"`
	Bots            int      `json:"bots"`
	VAC             bool     `json:"vac"`
	Versus          bool     `json:"versus"`
	Coop            bool     `json:"coop"`
	Ligting         string   `json:"lighting"`
	Mutated         bool     `json:"mutated"`
	Mutators        []string `json:"mutators"`
	GameMode        string   `json:"gameMode"`
	OfficialRuleset bool     `json:"officialRuleset"`
	CustomRules     bool     `json:"customRules"`
	Ranked          bool     `json:"ranked"`
	Moded           bool     `json:"moded"`
	ModsList        []string `json:"modsList"`
	Port            int      `json:"port"`
	GameID          int      `json:"gameId"`
	Version         string   `json:"version"`
}

// NetDto - Network counters
type NetDto struct {
	BytesIn  uint64 `json:"bytesIn"`
	BytesOut uint64 `json:"bytesOut"`
}

// MetricsDto - Dto for server metrics
type MetricsDto struct {
	PID        int32   `json:"pid"`
	CPUPercent float64 `json:"cpuPercent"`
	Command    string  `json:"command"`
	IsRunning  bool    `json:"isRunning"`
	MEMPercent float32 `json:"memPercent"`
	NumFDS     int32   `json:"numFDS"`
	NumThreads int32   `json:"numTreads"`
	Net        NetDto  `json:"net"`
}

// Dto - Runner Dto
type Dto struct {
	Server        *dtoservers.Dto `json:"server"`
	Settings      SettingsDto     `json:"settings"`
	Players       []*PlayersDto   `json:"players"`
	Metrics       MetricsDto      `json:"metrics"`
	Cmd           *exec.Cmd       `json:"cmd"`
	ExpectedToRun bool            `json:"expectedToRun"`
	Status        int             `json:"status"`
}

// Dtos - List of ScenarioModes Dto
type Dtos []*Dto

const (
	// StatusStopped - Runner is stopped
	StatusStopped = 0

	// StatusStarting - Runner is starting
	StatusStarting = 1

	// StatusStopping - Runner is stopping
	StatusStopping = 2

	// StatusRunning - Runner is running
	StatusRunning = 3

	// TimeoutRestart - time to wait for a runner to stop before starting it again (secs)
	TimeoutRestart = 30
)

var (
	//Data - ScenarioModes data
	Data             Dtos
	launchInCritical int = -1
)

// NewRunner - Creates a new Runner object with the server configuration
func NewRunner(server *dtoservers.Dto) *Dto {
	return &Dto{Server: server}
}

// Start - Starts a new runner process
func (runner *Dto) Start() {

	applog.AppLogs["ALPISSA"].Log("INFO", "Runners", fmt.Sprintf("Start request for server '%s' received", runner.Server.DisplayName))

	pos := -1

	for index, data := range Data {
		if runner.Server.ID == data.Server.ID {
			pos = index
			applog.AppLogs["ALPISSA"].Log("WARNING", "Runners", fmt.Sprintf("Server '%s' is already running start aborted", runner.Server.DisplayName))
			break
		}
	}

	if pos == -1 {
		launchInCritical = runner.Server.ID
		runner.Status = StatusStarting
		runner.ExpectedToRun = true

		secconds := 0
		for launchInCritical != -1 && launchInCritical != runner.Server.ID {
			time.Sleep(2 * time.Second)
			secconds += 2
			if secconds > TimeoutRestart {
				runner.ExpectedToRun = false
				runner.Status = StatusStopped
				applog.AppLogs["ALPISSA"].Log("ERROR", "Runners", fmt.Sprintf("Restart request for server '%s' to start timedout(%d secs waiting)", runner.Server.DisplayName, TimeoutRestart))
				return
			}
		}

		go func(runner *Dto, critical *int) {
			Launch(runner, critical)
			runner.Stop()
		}(runner, &launchInCritical)

		Data = append(Data, runner)
	}
}

// Restart - Restart a runner process
func (runner *Dto) Restart() {

	applog.AppLogs["ALPISSA"].Log("INFO", "Runners", fmt.Sprintf("Restart request for server '%s' received", runner.Server.DisplayName))

	doRestart := true
	secconds := 0

	if runner.Stop() == false {
		return
	}

	for runner.Status != 0 {
		time.Sleep(2 * time.Second)
		secconds += 2
		if secconds > TimeoutRestart {
			doRestart = false
			break
		}
	}

	if doRestart == true {
		runner.Start()
	} else {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Runners", fmt.Sprintf("Restart request for server '%s' restart timedout(%d secs waiting)", runner.Server.DisplayName, TimeoutRestart))
	}

}

// Stop - Stops a runner process
func (runner *Dto) Stop() bool {

	applog.AppLogs["ALPISSA"].Log("INFO", "Runners", fmt.Sprintf("Stop request for server '%s' received", runner.Server.DisplayName))

	pos := -1

	for index, data := range Data {
		if runner.Server.ID == data.Server.ID {
			pos = index
			break
		}
	}

	//TODO: Delete this line
	runner.ExpectedToRun = false
	applog.AppLogs["ALPISSA"].Log("INFO", "Runners", fmt.Sprintf("Server '%s' flaged to terminate", runner.Server.DisplayName))
	KillRunner(runner)

	if pos != -1 {
		Data[pos] = Data[len(Data)-1]
		Data[len(Data)-1] = nil
		Data = Data[:len(Data)-1]
	} else {
		applog.AppLogs["ALPISSA"].Log("WARNING", "Runners", fmt.Sprintf("Server '%s' is not running stop aborted", runner.Server.DisplayName))
		return false
	}

	return true
}

//TODO: Allow runners to stop, save and restart in case of reinstalation
