package dtoservers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtogamemodes"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtolighting"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtomutators"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoscenariomodes"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtosides"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

//IniValuesDto - Values for the INI files
type IniValuesDto struct {
	Name        string      `json:"name"`
	Type        string      `json:"type"`
	Value       interface{} `json:"value"`
	Description string      `json:"description"`
}

//MapCycleDto - Values for the MapCycle file
type MapCycleDto struct {
	Scenario string `json:"scenario"`
	Mode     string `json:"mode"`
	Lighting string `json:"lighting"`
}

// BanDto - Values for the BANS file
type BanDto struct {
	PlayerID  string `json:"playerId"`
	BanTime   uint64 `json:"banTime"`
	Duration  uint64 `json:"duration"`
	Banreason string `json:"banReason"`
	AdminID   string `json:"adminId"`
}

// INIFileDto - INI file data
type INIFileDto struct {
	Section     string         `json:"section"`
	Description string         `json:"description"`
	Variables   []IniValuesDto `json:"variables"`
}

// FilesDto - File information structure
type FilesDto struct {
	Game     []INIFileDto  `json:"game"`
	Engine   []INIFileDto  `json:"engine"`
	MapCycle []MapCycleDto `json:"mapcycle"`
	Admins   []string      `json:"admins"`
	Mods     []int32       `json:"mods"`
	Bans     []BanDto      `json:"bans"`
}

// PlayersDto - Players server information
type PlayersDto struct {
	Max     int `json:"max"`
	Visible int `json:"visible"`
}

// ServerDto - Dto for the general server configuration
type ServerDto struct {
	Name            string     `json:"name"`
	Password        string     `json:"password"`
	GamePort        int        `json:"gamePort"`
	QueryPort       int        `json:"queryPort"`
	IsHangRecovery  bool       `json:"isHangRecovery"`
	Gslt            string     `json:"gslt"`
	Stats           bool       `json:"stats"`
	Players         PlayersDto `json:"players"`
	ModIOToken      string     `json:"modIoToken"`
	AutoStart       bool       `json:"autoStart"`
	RefreshInterval int        `json:"refreshInterval"`
	DataPath        string     `json:"dataPath"`
}

// RConDto - Dto for the RCon server configration
type RConDto struct {
	Bind     string `json:"bind"`
	Port     int    `json:"port"`
	Password string `json:"password"`
	Enabled  bool   `json:"enabled"`
}

// GameDto - Dto for the server Game
type GameDto struct {
	DefaultMap          string   `json:"defaultMap"`
	DefaultScenario     string   `json:"defaultScenario"`
	DefaultMode         string   `json:"defaultMode"`
	DefaultLight        string   `json:"defaultLight"`
	DefaultSide         string   `json:"defaultSide"`
	GameMode            string   `json:"gameMode"`
	IsDefaultMapCustom  bool     `json:"isDefaultMapCustom"`
	IsDefaultModeCustom bool     `json:"isserverCustom"`
	IsGameModeCustom    bool     `json:"isGameModeCustom"`
	Mutators            []string `json:"mutators"`
	CustomMutators      []string `json:"customMutators"`
}

// StatsDto - Server statistics
type StatsDto struct {
	TimeRunning      int64 `json:"timeRunning"`
	TotalTimeRunning int64 `json:"totalTimeRunning"`
	TotalRounds      int64 `json:"totalRounds"`
}

// Dto - Server complete configration
type Dto struct {
	ID          int       `json:"id"`
	DisplayName string    `json:"displayName"`
	Server      ServerDto `json:"server"`
	RCon        RConDto   `json:"rcon"`
	Game        GameDto   `json:"game"`
	Files       *FilesDto `json:"files"`
	Stats       StatsDto  `json:"stats"`
}

// Dtos - List of Servers Dto
type Dtos []*Dto

const (
	// Dir - Servers directory
	Dir = "config"
	// File - Servers file name
	File = "Servers.json"

	minGamePort  = 7777
	minQueryPort = 27131
	minRConPort  = 27015
	localhost    = "127.0.0.1"
)

var (
	//Data - Servers data
	Data  Dtos
	maxID int
)

// Load - Loads the file data.
func Load() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Servers", "Loading Servers configuration")

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return fmt.Errorf("file '%s' doesn't exist", path)
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	return nil
}

// Save - Saves a Mutator object
func (server *Dto) Save() *httperrors.HTTPError {

	// Validates the incomming data
	if errValidate := server.Validate(); errValidate != nil {
		return errValidate
	}

	if server.ID == 0 {
		// New object
		// Increments the ID
		maxID++
		// Assigns the new ID to the new object
		server.ID = maxID
		// Inserts the new object into the Data array
		Data = append(Data, server)
	} else {
		// Existing object
		var found bool = false
		// Loop through the array
		for index, data := range Data {
			// Saerches for the existing object by its ID
			if server.ID == data.ID {
				// Assigns the new data to the object position in the array
				Data[index] = server
				// Flags that the object was found
				found = true
				// Breaks the loop
				break
			}
		}

		// If the object was not found
		if found == false {
			// Return not found error
			return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
		}
	}

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Delete - Deletes a Mutator object
func (server *Dto) Delete() *httperrors.HTTPError {

	// Finds the object in the array by its ID
	var pos int = -1
	for index, data := range Data {
		if server.ID == data.ID {
			pos = index
			break
		}
	}

	if pos == -1 {
		// If the ID wasn't found return a not found error
		return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
	}

	// Removes the object from the array shifting it's position so it will not be empty
	Data[pos] = Data[len(Data)-1]
	Data[len(Data)-1] = nil
	Data = Data[:len(Data)-1]

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Validate - Validates a Server object
func (server *Dto) Validate() *httperrors.HTTPError {

	// Trim any leftover spaces
	server.DisplayName = strings.TrimSpace(server.DisplayName)

	// Checks if the required field value is empty
	if server.DisplayName == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "displayName", Message: "the field displayName is required"}
	}

	// Checks if the server.server sub-document is valid
	if errServer := server.Server.ValidateServer(); errServer != nil {
		return errServer
	}

	// Checks if the server.rcon sub-document is valid
	if errRCon := server.RCon.ValidateRcon(); errRCon != nil {
		return errRCon
	}

	// Check if the server.game sub-document is valid
	if errGame := server.Game.ValidateGame(); errGame != nil {
		return errGame
	}

	// Check if the server displayName already exists
	for _, item := range Data {
		if item.DisplayName == server.DisplayName {
			return &httperrors.HTTPError{Code: 409, Context: "server.displayName", Message: "the server.displayName is already assigned to another configuration"}
		}
	}

	return nil
}

// ValidateServer - Validates a Server sub-document object
func (server *ServerDto) ValidateServer() *httperrors.HTTPError {

	// Check the server host name
	server.Name = strings.TrimSpace(server.Name)
	if server.Name == "" {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.name", Message: "the field server.server.name is required"}
	}

	// Trim the server password of leftover spaces
	server.Password = strings.TrimSpace(server.Password)

	// Checks the server game port
	if server.GamePort < minGamePort {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.gamePort", Message: fmt.Sprintf("the field server.server.gamePort can't be lower than %d", minGamePort)}
	}

	// Checks the server query port
	if server.QueryPort < minQueryPort {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.queryPort", Message: fmt.Sprintf("the field server.server.queryPort can't be lower than %d", minQueryPort)}
	}

	// Checks the server max players
	if server.Players.Max < 1 {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.players.max", Message: "the field server.server.players.max can't be lower than 1"}
	}

	// Checks the server visible players
	if server.Players.Visible < 1 {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.players.visible", Message: "the field server.server.players.visible can't be lower than 1"}
	}

	// Checks if the server max player is smaller than visible players
	if server.Players.Max < server.Players.Visible {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.players", Message: "the field server.server.players.max can't be lower than server.server.players.visible"}
	}

	// Checks the server Steam Game Server Login Token and if the user wnats stats
	server.Gslt = strings.TrimSpace(server.Gslt)
	if server.Gslt == "" && server.Stats == true {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.gslt", Message: "it's impossible to have a stats server withou a Steam GSLT(Game Server Login Token)"}
	}

	// Trim the server mod.io token of leftover spaces
	server.ModIOToken = strings.TrimSpace(server.ModIOToken)

	// Checks the server refresh interval
	if server.RefreshInterval < 1000 {
		return &httperrors.HTTPError{Code: 400, Context: "server.server.refreshInterval", Message: "the field server.server.refreshInterval can't be lower than 1000"}
	}

	// Checks for conflicts
	for _, item := range Data {

		// Checks if the server name already exists
		if server.Name == item.Server.Name {
			return &httperrors.HTTPError{Code: 409, Context: "server.server.name", Message: "the server.server.name is already assigned to another configuration"}
		}

		// Checks if the server game port already exists
		if server.GamePort == item.Server.GamePort {
			return &httperrors.HTTPError{Code: 409, Context: "server.server.gamePort", Message: "the server.server.gamePort is already assigned to another configuration"}
		}

		// Checks if the server query port already exists
		if server.QueryPort == item.Server.QueryPort {
			return &httperrors.HTTPError{Code: 409, Context: "server.server.queryPort", Message: "the server.server.queryPort is already assigned to another configuration"}
		}
	}

	return nil
}

// ValidateRcon - Validates a Rcon sub-document object
func (rcon *RConDto) ValidateRcon() *httperrors.HTTPError {

	// Trim the server rcon bind address of leftover spaces
	rcon.Bind = strings.TrimSpace(rcon.Bind)
	if rcon.Enabled == false {

		// If the Rcon will not be enabled we enable it for
		// the localhost only as we need it to communicate with the server
		rcon.Bind = localhost
	}

	// Check the RCon port
	if rcon.Port < minRConPort {
		return &httperrors.HTTPError{Code: 400, Context: "server.rcon.port", Message: fmt.Sprintf("the field server.server.queryPort can't be lower than %d", minRConPort)}
	}

	return nil
}

// ValidateGame - Validates a Game sub-document object
func (game *GameDto) ValidateGame() *httperrors.HTTPError {

	// Check the game default map
	game.DefaultMap = strings.TrimSpace(game.DefaultMap)
	if game.DefaultMap == "" {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultMap", Message: "the field server.game.defaultMap is required"}
	}

	// Check the game default scenario
	game.DefaultScenario = strings.TrimSpace(game.DefaultScenario)
	if game.DefaultScenario == "" {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultScenario", Message: "the field server.game.defaultScenario is required"}
	}

	// Check the game default mode
	game.DefaultMode = strings.TrimSpace(game.DefaultMode)
	if game.DefaultScenario == "" && game.IsDefaultModeCustom == false {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultMode", Message: "the field server.game.defaultMode is required if not isDefaultModeCustom"}
	}

	// Check the if game default mode exists and it's not custom
	if game.IsDefaultModeCustom == false {
		found := true
		if len(dtoscenariomodes.Data) > 0 {
			for _, scenariomode := range dtoscenariomodes.Data {
				if game.DefaultMode == scenariomode.GameName {
					found = true
					break
				}
			}
			if found == false {
				return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultMode", Message: "the field server.game.defaultMode isn't valid"}
			}
		}
	}

	// Checks game lighting
	game.DefaultLight = strings.TrimSpace(game.DefaultLight)
	if game.DefaultLight == "" {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultLight", Message: "the field server.game.defaultLight is required"}
	}

	// Checks game lighting exists
	var found = true
	if len(dtolighting.Data) > 0 {
		for _, lighting := range dtolighting.Data {
			if game.DefaultLight == lighting.GameName {
				found = true
				break
			}
		}
		if found == false {
			return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultLight", Message: "the field server.game.defaultLight isn't valid"}
		}
	}

	game.DefaultSide = strings.TrimSpace(game.DefaultSide)
	if game.DefaultSide == "" {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultSide", Message: "the field server.game.defaultSide is required"}
	}

	found = true
	if len(dtosides.Data) > 0 {
		for _, side := range dtosides.Data {
			if game.DefaultSide == side.GameName {
				found = true
				break
			}
		}
		if found == false {
			return &httperrors.HTTPError{Code: 400, Context: "server.game.defaultSide", Message: "the field server.game.defaultSide isn't valid"}
		}
	}

	// Checks game mode
	game.GameMode = strings.TrimSpace(game.GameMode)
	if game.GameMode == "" && game.IsGameModeCustom == false {
		return &httperrors.HTTPError{Code: 400, Context: "server.game.gameMode", Message: "the field server.game.gameMode is required if not isGameModeCustom"}
	}

	// Checks game mode and if its not custom
	if game.IsGameModeCustom == false {

		found = true
		if len(dtogamemodes.Data) > 0 {
			for _, gamemode := range dtogamemodes.Data {
				if game.GameMode == gamemode.GameName {
					found = true
					break
				}
			}
			if found == false {
				return &httperrors.HTTPError{Code: 400, Context: "server.game.gamemode", Message: "the field server.game.gamemode isn't valid"}
			}
		}
	}

	// Checks if game mutators exist
	found = false
	if len(game.Mutators) > 0 && len(dtomutators.Data) > 0 {

		for index, mutator := range game.Mutators {
			found = false

			for _, item := range dtomutators.Data {
				if mutator == item.GameName {
					found = true
					break
				}
			}

			if found == false {
				return &httperrors.HTTPError{Code: 400, Context: fmt.Sprintf("server.game.mutators[%d]", index), Message: fmt.Sprintf("the field mutator[%d] has a mutator named '%s' that isn't valid", index, mutator)}
			}
		}
	}

	return nil
}

func save() *httperrors.HTTPError {

	// Marshal the array data into JSON
	jsonString, errMarshal := json.MarshalIndent(Data, "", "    ")
	if errMarshal != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: "failed to marshal to JSON"}
	}

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("file '%s' doesn't exist", path)}
	}

	// Writes the file
	if errWrite := ioutil.WriteFile(path, jsonString, 0644); errWrite != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("failed to save file %s. ERROR: %s", path, errWrite.Error())}
	}

	return nil
}
