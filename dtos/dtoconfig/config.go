package dtoconfig

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"bitbucket.org/joaorsilva/alpissa/utils"
)

// DtoConfig - Configure Dto
type DtoConfig struct {
	Address         string `json:"address"`
	Port            int    `json:"port"`
	SteamPath       string `json:"steamPath"`
	IntancesPath    string `json:"intancesPath"`
	AppID           int    `json:"appId"`
	SteamAPYKey     string `json:"steamAPYKey"`
	SteamCmdURL     string `json:"steamCmdUrl"`
	SteamCmdURLFile string `json:"steamCmdUrlFile"`
	LogPath         string `json:"logPath"`
	AppPath         string `json:"appPath"`
	BaseDir         string `json:"baseDir"`
}

var (
	// Data - Configuration data
	Data DtoConfig
)

const (

	// Dir - Config directory
	Dir = "config"
	// File - Config filename
	File = "config.json"
)

// Load - Loads the configuration file data.
func Load(basePath string) error {

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return errors.New("file doesn't exist")
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	Data.BaseDir = basePath

	return nil
}
