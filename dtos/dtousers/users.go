package dtousers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoplayers"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/utils"
	"bitbucket.org/joaorsilva/alpissa/utils/validators"
)

// Dto - Users Dto
type Dto struct {
	ID       int                 `json:"id"`
	Username string              `json:"username"`
	Email    string              `json:"email"`
	Password string              `json:"password"`
	Steam    dtoplayers.SteamDto `json:"steam"`
}

// Dtos - List of Users Dto
type Dtos []*Dto

const (
	// Dir - Users directory
	Dir = "config"
	// File - Users file name
	File = "Users.json"
)

var (
	//Data - Users data
	Data  Dtos
	maxID int
)

// Load - Loads the file data.
func Load() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Users", "Loading Users configuration")

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return fmt.Errorf("file '%s' doesn't exist", path)
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	return nil
}

// Save - Saves a Scenario Mode object
func (user *Dto) Save() *httperrors.HTTPError {

	// Validates the incomming data
	if errValidate := user.Validate(); errValidate != nil {
		return errValidate
	}

	if user.ID == 0 {
		// New object
		// Increments the ID
		maxID++

		// Assigns the new ID to the new object
		user.ID = maxID

		// Encrypt password
		if user.Password != "" {
			var errEncrypt error
			user.Password, errEncrypt = utils.HashString(user.Password)
			if errEncrypt != nil {
				return &httperrors.HTTPError{Code: 404, Context: "document", Message: fmt.Sprintf("password encryption error. ERROR: %s", errEncrypt.Error())}
			}
		}

		// Inserts the new object into the Data array
		Data = append(Data, user)

	} else {
		// Existing object
		var found bool = false
		// Loop through the array
		for index, data := range Data {
			// Saerches for the existing object by its ID
			if user.ID == data.ID {

				// If password is sent then ecrypt it
				if user.Password != "" {
					var errEncrypt error
					user.Password, errEncrypt = utils.HashString(user.Password)
					if errEncrypt != nil {
						return &httperrors.HTTPError{Code: 404, Context: "document", Message: fmt.Sprintf("password encryption error. ERROR: %s", errEncrypt.Error())}
					}
				} else {
					// If password is not sent then use the old one
					user.Password = data.Password
				}

				// Assigns the new data to the object position in the array
				Data[index] = user
				// Flags that the object was found
				found = true
				// Breaks the loop
				break
			}
		}

		// If the object was not found
		if found == false {
			// Return not found error
			return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
		}
	}

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Delete - Deletes a User object
func (side *Dto) Delete() *httperrors.HTTPError {

	// Finds the object in the array by its ID
	var pos int = -1
	for index, data := range Data {
		if side.ID == data.ID {
			pos = index
			break
		}
	}

	if pos == -1 {
		// If the ID wasn't found return a not found error
		return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
	}

	// Removes the object from the array shifting it's position so it will not be empty
	Data[pos] = Data[len(Data)-1]
	Data[len(Data)-1] = nil
	Data = Data[:len(Data)-1]

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Validate - Validates a Scenario Mode object
func (user *Dto) Validate() *httperrors.HTTPError {

	user.Username = strings.TrimSpace(user.Username)
	if user.Username == "" {
		return &httperrors.HTTPError{Code: 400, Context: "username", Message: "the field username is required"}
	}

	user.Password = strings.TrimSpace(user.Password)
	if user.Password == "" && user.ID == 0 {
		return &httperrors.HTTPError{Code: 400, Context: "username", Message: "the field password is required"}
	}

	user.Email = strings.TrimSpace(user.Email)
	if user.Email == "" {
		return &httperrors.HTTPError{Code: 400, Context: "email", Message: "the field email is required"}
	}

	email := validators.NewEmail(user.Email)
	if email.IsValid() != true {
		return &httperrors.HTTPError{Code: 400, Context: "email", Message: "the field email is not valid"}
	}

	return nil
}

func save() *httperrors.HTTPError {

	// Marshal the array data into JSON
	jsonString, errMarshal := json.MarshalIndent(Data, "", "    ")
	if errMarshal != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: "failed to marshal to JSON"}
	}

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("file '%s' doesn't exist", path)}
	}

	// Writes the file
	if errWrite := ioutil.WriteFile(path, jsonString, 0644); errWrite != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("failed to save file %s. ERROR: %s", path, errWrite.Error())}
	}

	return nil
}
