package dtomutators

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

// Dto - Mutators Dto
type Dto struct {
	ID          int    `json:"id"`
	DisplayName string `json:"displayName"`
	GameName    string `json:"gameName"`
	Decription  string `json:"description"`
}

// Dtos - List of Mutators Dto
type Dtos []*Dto

const (
	// Dir - Mutators directory
	Dir = "config"
	// File - Mutators file name
	File = "Mutators.json"
)

var (
	//Data - Game Modes data
	Data  Dtos
	maxID int
)

// Load - Loads the file data.
func Load() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Mutators", "Loading Mutators configuration")

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return fmt.Errorf("file '%s' doesn't exist", path)
	}

	// Opens the file
	file, errFile := os.Open(path)
	if errFile != nil {
		return errFile
	}

	// Defer closing the file
	defer file.Close()

	// Creates a new JSON decoder for the file
	jsonDecoder := json.NewDecoder(file)

	// Decodes the JSON in the file into the Dto structure
	if errDecode := jsonDecoder.Decode(&Data); errDecode != nil {
		return errDecode
	}

	return nil
}

// Save - Saves a Mutator object
func (mutator *Dto) Save() *httperrors.HTTPError {

	// Validates the incomming data
	if errValidate := mutator.Validate(); errValidate != nil {
		return errValidate
	}

	if mutator.ID == 0 {
		// New object
		// Increments the ID
		maxID++
		// Assigns the new ID to the new object
		mutator.ID = maxID
		// Inserts the new object into the Data array
		Data = append(Data, mutator)
	} else {
		// Existing object
		var found bool = false
		// Loop through the array
		for index, data := range Data {
			// Saerches for the existing object by its ID
			if mutator.ID == data.ID {
				// Assigns the new data to the object position in the array
				Data[index] = mutator
				// Flags that the object was found
				found = true
				// Breaks the loop
				break
			}
		}

		// If the object was not found
		if found == false {
			// Return not found error
			return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
		}
	}

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Delete - Deletes a Mutator object
func (mutator *Dto) Delete() *httperrors.HTTPError {

	// Finds the object in the array by its ID
	var pos int = -1
	for index, data := range Data {
		if mutator.ID == data.ID {
			pos = index
			break
		}
	}

	if pos == -1 {
		// If the ID wasn't found return a not found error
		return &httperrors.HTTPError{Code: 404, Context: "document", Message: "not found"}
	}

	// Removes the object from the array shifting it's position so it will not be empty
	Data[pos] = Data[len(Data)-1]
	Data[len(Data)-1] = nil
	Data = Data[:len(Data)-1]

	// Saves the array
	if errSave := save(); errSave != nil {
		return errSave
	}

	return nil
}

// Validate - Validates a Mutator object
func (mutator *Dto) Validate() *httperrors.HTTPError {

	// Trim any leftover spaces
	mutator.DisplayName = strings.TrimSpace(mutator.DisplayName)

	// Checks if the required field value is empty
	if mutator.DisplayName == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "displayName", Message: "the field displayName is required"}
	}

	// Trim any leftover spaces
	mutator.GameName = strings.TrimSpace(mutator.GameName)

	// Checks if the required field value is empty
	if mutator.GameName == "" {
		// Returns a bad request error
		return &httperrors.HTTPError{Code: 400, Context: "gameName", Message: "the field GameName is required"}
	}

	// Trim any leftover spaces
	mutator.Decription = strings.TrimSpace(mutator.Decription)

	return nil
}

func save() *httperrors.HTTPError {

	// Marshal the array data into JSON
	jsonString, errMarshal := json.MarshalIndent(Data, "", "    ")
	if errMarshal != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: "failed to marshal to JSON"}
	}

	// Gets the base path
	basePath := dtoconfig.Data.BaseDir

	// Builds the full file path
	path := fmt.Sprintf("%s/%s/%s", basePath, Dir, File)

	// Checks if the file exists
	if utils.FileExists(path) == false {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("file '%s' doesn't exist", path)}
	}

	// Writes the file
	if errWrite := ioutil.WriteFile(path, jsonString, 0644); errWrite != nil {
		return &httperrors.HTTPError{Code: 500, Context: "document", Message: fmt.Sprintf("failed to save file %s. ERROR: %s", path, errWrite.Error())}
	}

	return nil
}
