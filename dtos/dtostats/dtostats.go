package dtostats

import (
	"fmt"
	"time"

	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/services/serverstats"
)

// CPUDto - DTO for CPU table
type CPUDto struct {
	ID        int    `json:"id"`
	Dt        string `json:"dt"`
	CPUCount  uint64 `json:"cpuCount"`
	Guest     uint64 `json:"guest"`
	GuestNice uint64 `json:"gestNice"`
	Idle      uint64 `json:"idle"`
	Iowait    uint64 `json:"iowait"`
	Irq       uint64 `json:"irq"`
	Nice      uint64 `json:"nice"`
	Softirq   uint64 `json:"softIrq"`
	StatCount uint64 `json:"stateCount"`
	Steal     uint64 `json:"steal"`
	System    uint64 `json:"system"`
	Total     uint64 `json:"total"`
	User      uint64 `json:"user"`
}

// MEMDto - DTO for Mem table
type MEMDto struct {
	ID         int    `json:"id"`
	Dt         string `json:"dt"`
	Available  uint64 `json:"avaliable"`
	Buffers    uint64 `json:"buffers"`
	Cached     uint64 `json:"cached"`
	Free       uint64 `json:"free"`
	Inactive   uint64 `json:"inactive"`
	Enabled    uint64 `json:"enabled"`
	SwapCached uint64 `json:"swapCached"`
	SwapFree   uint64 `json:"swapFree"`
	SwapTotal  uint64 `json:"swapTotal"`
	SwapUsed   uint64 `json:"swapUsed"`
	Total      uint64 `json:"total"`
	Used       uint64 `json:"used"`
}

// GetCPU - Gets the CPU statistics from the database based on the date/time intervals
func GetCPU(startTime string, endTime string) (*[]CPUDto, *httperrors.HTTPError) {

	stmt, errStmt := serverstats.Database.Prepare("SELECT * FROM cpu WHERE dateandtime >=? AND dateandtime <=?")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Stats", fmt.Sprintf("Failed prepare cpu query. ERROR: %s", errStmt.Error()))
		return nil, &httperrors.HTTPError{Code: 500, Context: "Database", Message: "Failed prepare cpu query"}
	}

	var cpus []CPUDto

	now := time.Now()
	if startTime == "" {
		startTime = fmt.Sprintf("%04d-%02d-%02dT00:00:00Z", now.Year(), now.Month(), now.Day())
	}

	if endTime == "" {
		endTime = fmt.Sprintf("%04d-%02d-%02dT23:59:59Z", now.Year(), now.Month(), now.Day())
	}

	result, errExec := stmt.Query(startTime, endTime)
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Stats", fmt.Sprintf("Error reading database. ERROR: %s", errExec.Error()))
		return nil, &httperrors.HTTPError{Code: 500, Context: "Database", Message: "Error reading database"}
	}

	for result.Next() {
		stat := CPUDto{}
		result.Scan(&stat.ID, &stat.Dt, &stat.CPUCount, &stat.Guest, &stat.GuestNice, &stat.Idle, &stat.Iowait, &stat.Irq, &stat.Nice, &stat.Softirq, &stat.StatCount, &stat.Steal, &stat.System, &stat.Total, &stat.User)
		cpus = append(cpus, stat)
	}

	return &cpus, nil
}

// GetMem - Gets the Memory statistics from the database based on the date/time intervals
func GetMem(startTime string, endTime string) (*[]MEMDto, *httperrors.HTTPError) {

	stmt, errStmt := serverstats.Database.Prepare("SELECT * FROM mem WHERE dateandtime >=? AND dateandtime <=?")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Stats", fmt.Sprintf("Failed prepare mem query. ERROR: %s", errStmt.Error()))
		return nil, &httperrors.HTTPError{Code: 500, Context: "Database", Message: "Failed to prepare mem query"}
	}

	var mems []MEMDto

	now := time.Now()
	if startTime == "" {
		startTime = fmt.Sprintf("%04d-%02d-%02dT00:00:00Z", now.Year(), now.Month(), now.Day())
	}

	if endTime == "" {
		endTime = fmt.Sprintf("%04d-%02d-%02dT23:59:59Z", now.Year(), now.Month(), now.Day())
	}

	result, errExec := stmt.Query(startTime, endTime)
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Stats", fmt.Sprintf("Error reading database. ERROR: %s", errExec.Error()))
		return nil, &httperrors.HTTPError{Code: 500, Context: "Database", Message: "Error reading database"}
	}

	for result.Next() {
		stat := MEMDto{}
		result.Scan(&stat.ID, &stat.Dt, &stat.Available, &stat.Buffers, &stat.Cached, &stat.Enabled, &stat.Free, &stat.Inactive, &stat.SwapCached, &stat.SwapFree, &stat.SwapTotal, &stat.SwapUsed, &stat.Total, &stat.Used)
		mems = append(mems, stat)
	}

	return &mems, nil
}
