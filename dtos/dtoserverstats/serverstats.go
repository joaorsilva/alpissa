package dtoserverstats

import (
	"time"

	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/disk"
	"github.com/mackerelio/go-osstat/loadavg"
	"github.com/mackerelio/go-osstat/memory"
	"github.com/mackerelio/go-osstat/network"
)

// Dto - Dto struct for system stats
type Dto struct {
	Time   time.Time
	CPU    *cpu.Stats
	Mem    *memory.Stats
	Net    []network.Stats
	FS     []disk.Stats
	Avg    *loadavg.Stats
	UpTime time.Duration
}

// Dtos - List of a system stats
type Dtos []*Dto

var (
	// Data - Last collected Dto
	Data Dto

	// Datas - 1 Hour colelcted Dtos
	Datas Dtos
)
