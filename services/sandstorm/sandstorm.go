package sandstorm

import (
	"fmt"
	"os"
	"os/exec"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/steamcmd"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtostatus"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

// Check - Check if Sandstorm is instaleld
func Check() bool {

	applog.AppLogs["ALPISSA"].Log("INFO", "Sandstorm", "Checking if the Sandstorm server is instaleld...")
	dtostatus.Data.Sandstorm.IsInstaled = false

	if utils.FileExists(fmt.Sprintf("%s/%s/sandstorm-server/Insurgency/Binaries/Linux/InsurgencyServer-Linux-Shipping", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath)) == false {
		applog.AppLogs["ALPISSA"].Log("WARNING", "Sandstorm", "Sanstorm server is not instaleld.")
		return false
	}

	dtostatus.Data.Sandstorm.IsInstaled = true
	applog.AppLogs["ALPISSA"].Log("INFO", "Sandstorm", "Sanstorm server is instaleld.")
	return true
}

// Update - Install or update Sandstorm
func Update() bool {

	applog.AppLogs["ALPISSA"].Log("INFO", "Sandstorm", "Installing/Updating Sandstorm server.")

	dtostatus.Data.Sandstorm.IsInstaled = false
	dtostatus.Data.Sandstorm.IsInstaling = true

	cmd := exec.Command(
		fmt.Sprintf("%s/%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, steamcmd.SteamCmdCommand),
		"+login anonymous",
		fmt.Sprintf("+force_install_dir %s/%s/sandstorm-server", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath),
		fmt.Sprintf("+app_update %d", dtoconfig.Data.AppID),
		"+quit")

	if err := cmd.Run(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Sandstorm", fmt.Sprintf("Failed to run SteamCMD (check if you have the SteamCMD requirements installed in yor system). ERROR: %s", err.Error()))
		dtostatus.Data.Sandstorm.IsInstaling = false
		return false
	}

	cmd.Wait()

	dtostatus.Data.Sandstorm.IsInstaling = false

	return true
}

// Remove - Remove Sandstorm instalation
func Remove() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Sandstorm", "Removing Sandstorm instalation.")
	errRemove := os.RemoveAll(fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath))
	if errRemove != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Sandstorm", fmt.Sprintf("Error removing Sandstorm. ERROR: %s", errRemove.Error()))
		return errRemove
	}

	dtostatus.Data.Sandstorm.IsInstaled = false

	applog.AppLogs["ALPISSA"].Log("INFO", "Sandstorm", "Sandstorm instalation removed.")

	return steamcmd.MakeDirs()
}
