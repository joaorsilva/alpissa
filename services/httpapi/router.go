package httpapi

import (
	"bitbucket.org/joaorsilva/alpissa/controllers"
	"github.com/gin-gonic/gin"
)

// SetRoutes - Sets the HTTP API routes
func SetRoutes(router *gin.Engine) {

	// GameModes
	router.GET("/gamemodes", controllers.GameModesList)
	router.GET("/gamemodes/:id", controllers.GameModesGet)
	router.POST("/gamemodes", controllers.GameModesCreate)
	router.PUT("/gamemodes/:id", controllers.GameModesUpdate)
	router.DELETE("/gamemodes/:id", controllers.GameModesDelete)

	// Lighting
	router.GET("/lighting", controllers.LightingList)
	router.GET("/lighting/:id", controllers.LightingGet)
	router.POST("/lighting", controllers.LightingCreate)
	router.PUT("/lighting/:id", controllers.LightingUpdate)
	router.DELETE("/lighting/:id", controllers.LightingDelete)

	// Mutators
	router.GET("/mutators", controllers.MutatorsList)
	router.GET("/mutators/:id", controllers.MutatorsGet)
	router.POST("/mutators", controllers.MutatorsCreate)
	router.PUT("/mutators/:id", controllers.MutatorsUpdate)
	router.DELETE("/mutators/:id", controllers.MutatorsDelete)

	// Players
	router.GET("/players", controllers.PlayersList)
	router.GET("/players/:id", controllers.PlayersGet)
	router.POST("/players", controllers.PlayersCreate)
	router.PUT("/players/:id", controllers.PlayersUpdate)
	router.DELETE("/players/:id", controllers.PlayersDelete)

	// Runners
	router.GET("/runners", controllers.RunnersList)
	router.GET("/runners/:id", controllers.RunnersGet)
	router.POST("/runners/:id", controllers.RunnersStart)
	router.PUT("/runners/:id", controllers.RunnersRestart)
	router.DELETE("/runners/:id", controllers.RunnersStop)

	// ScenarioModes
	router.GET("/scenariomodes", controllers.ScenarioModesList)
	router.GET("/scenariomodes/:id", controllers.ScenarioModesGet)
	router.POST("/scenariomodes", controllers.ScenarioModesCreate)
	router.PUT("/scenariomodes/:id", controllers.ScenarioModesUpdate)
	router.DELETE("/scenariomodes/:id", controllers.ScenarioModesDelete)

	// Servers
	router.GET("/servers", controllers.ServersList)
	router.GET("/servers/:id", controllers.ServersGet)
	router.POST("/servers", controllers.ServersCreate)
	router.PUT("/servers/:id", controllers.ServersUpdate)
	router.DELETE("/servers/:id", controllers.ServersDelete)

	// Sides
	router.GET("/sides", controllers.SidesList)
	router.GET("/sides/:id", controllers.SidesGet)
	router.POST("/sides", controllers.SidesCreate)
	router.PUT("/sides/:id", controllers.SidesUpdate)
	router.DELETE("/sides/:id", controllers.SidesDelete)

	// Users
	router.GET("/users", controllers.UsersList)
	router.GET("/users/:id", controllers.UsersGet)
	router.POST("/users", controllers.UsersCreate)
	router.PUT("/users/:id", controllers.UsersUpdate)
	router.DELETE("/users/:id", controllers.UsersDelete)

	// Status
	router.GET("/status", controllers.StatusGet)

	// Stats
	router.GET("/stats/cpu", controllers.StatsCPU)
	router.GET("/stats/mem", controllers.StatsMem)

	// Steam
	router.POST("/steam", controllers.SteamInstall)
	router.PUT("/steam", controllers.SteamUpdate)
	router.DELETE("/steam", controllers.SteamDelete)

	// Sandstorm

}
