package httpapi

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"

	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"github.com/gin-gonic/gin"
)

// NewHTTP - Created a new the HTTP server
func NewHTTP() *gin.Engine {

	gin.SetMode(gin.DebugMode)
	svr := gin.Default()
	return svr
}

// Listen - Start server listenning
func Listen(router *gin.Engine) *http.Server {

	// Builds the native HTTP struct
	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", dtoconfig.Data.Address, dtoconfig.Data.Port),
		Handler: router,
	}

	go func() {
		// According to gin-gonic docs it's th right way to do it in order to grafully shutdown (https://github.com/gin-gonic/examples/blob/master/graceful-shutdown/graceful-shutdown/server.go)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			applog.AppLogs["ALPISSA"].Log("INFO", "HTTP API", fmt.Sprintf("HTTP API Server failed to listen on %s:%d. ERROR: %s", dtoconfig.Data.Address, dtoconfig.Data.Port, err.Error()))
			log.Fatalf("[  HTTP   ] - Error listenning on interface %s:%d - %s", dtoconfig.Data.Address, dtoconfig.Data.Port, err.Error())
		}
	}()

	applog.AppLogs["ALPISSA"].Log("INFO", "HTTP API", fmt.Sprintf("HTTP API Server is listenning on %s:%d", dtoconfig.Data.Address, dtoconfig.Data.Port))

	log.Printf("HTTP API Started on %s:%d\n", dtoconfig.Data.Address, dtoconfig.Data.Port)

	return srv
}
