package httphelpers

import (
	"strconv"

	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"github.com/gin-gonic/gin"
)

// GetID - Gets the ID parameter from the URL
func GetID(c *gin.Context) (int, *httperrors.HTTPError) {

	paramID := c.Param("id")

	ID, err := strconv.Atoi(paramID)
	if err != nil {
		return 0, &httperrors.HTTPError{Code: 400, Context: "Request", Message: "Invalid id parameter"}
	}
	return ID, nil
}

// GetJSONBody - Gets the JSON data from the body
func GetJSONBody(c *gin.Context, dto interface{}) *httperrors.HTTPError {

	if errBind := c.ShouldBindJSON(&dto); errBind != nil {
		return &httperrors.HTTPError{Code: 400, Context: "Request", Message: "Invalid JSON Data"}
	}

	return nil
}
