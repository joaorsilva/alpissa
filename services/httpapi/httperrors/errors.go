package httperrors

type HTTPError struct {
	Code    int    `json:"code"`
	Context string `json:"context"`
	Message string `json:"message"`
}
