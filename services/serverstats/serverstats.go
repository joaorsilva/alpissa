package serverstats

import (
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoserverstats"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/disk"
	"github.com/mackerelio/go-osstat/loadavg"
	"github.com/mackerelio/go-osstat/memory"
	"github.com/mackerelio/go-osstat/network"

	_ "github.com/mattn/go-sqlite3"
)

var (
	Database   *sql.DB
	insideSave bool
	isRunning  bool
	stmtCPU    *sql.Stmt
	stmtMem    *sql.Stmt
	stmtNet    *sql.Stmt
	stmtFS     *sql.Stmt
	stmtLoad   *sql.Stmt
)

// Start - Starts the sverer stats loop
func Start() {

	applog.AppLogs["ALPISSA"].Log("INFO", "ServerStats", "Connecting with stats database.")
	database, errDatabase := sql.Open("sqlite3", fmt.Sprintf("%s/%s/stats.db", dtoconfig.Data.BaseDir, dtoconfig.Data.LogPath))
	if errDatabase != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed open Stats database. ERROR: %s", errDatabase.Error()))
		return
	}

	applog.AppLogs["ALPISSA"].Log("INFO", "ServerStats", "Stats database connected.")

	Database = database

	insideSave = true
	if errCreate := createTables(database); errCreate != nil {
		insideSave = false
		database.Close()
		Database = nil
		return
	}

	if errPrepare := prepareStatements(database); errPrepare != nil {
		insideSave = false
		database.Close()
		Database = nil
		return
	}
	insideSave = false

	isRunning = true
	collector()
}

// Stop - Stops the sverer stats loop
func Stop() {

	isRunning = false
	for insideSave == true {
		applog.AppLogs["ALPISSA"].Log("INFO", "ServerStats", "Waiting threads to stop database activity...")
		time.Sleep(1 * time.Second)
	}
	applog.AppLogs["ALPISSA"].Log("INFO", "ServerStats", "Closing stats database connection.")
	Database.Close()
	applog.AppLogs["ALPISSA"].Log("INFO", "ServerStats", "Stats database connection closed.")
	Database = nil
}

func collector() {

	var errCPU error
	var errMem error
	var errNet error
	var errFS error
	var errAVG error

	go func() {
		for isRunning == true {

			dtoserverstats.Data.CPU, errCPU = cpu.Get()
			if errCPU != nil {
				applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to get CPU stats. ERROR: %s", errCPU.Error()))
			}

			dtoserverstats.Data.Mem, errMem = memory.Get()
			if errMem != nil {
				applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to get Memory stats. ERROR: %s", errMem.Error()))
			}

			dtoserverstats.Data.Net, errNet = network.Get()
			if errNet != nil {
				applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to get Network stats. ERROR: %s", errNet.Error()))
			}

			dtoserverstats.Data.FS, errFS = disk.Get()
			if errFS != nil {
				applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to get File System stats. ERROR: %s", errFS.Error()))
			}

			dtoserverstats.Data.Avg, errAVG = loadavg.Get()
			if errAVG != nil {
				applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to get Load Avarage stats. ERROR: %s", errAVG.Error()))
			}

			dtoserverstats.Data.Time = time.Now().UTC()

			insideSave = true
			if Database != nil {
				saveStats(&dtoserverstats.Data)
			}
			insideSave = false

			time.Sleep(10 * time.Second)
		}
	}()
}

func saveStats(stats *dtoserverstats.Dto) {

	now := fmt.Sprintf(dtoserverstats.Data.Time.Format("2006-01-02T15:04:05Z"))

	_, errInsert := stmtCPU.Exec(now, stats.CPU.CPUCount, stats.CPU.Guest, stats.CPU.GuestNice, stats.CPU.Idle, stats.CPU.Iowait, stats.CPU.Irq, stats.CPU.Nice, stats.CPU.Softirq, stats.CPU.StatCount, stats.CPU.Steal, stats.CPU.System, stats.CPU.Total, stats.CPU.User)
	if errInsert != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to insert 'cpu' data. ERROR: %s", errInsert.Error()))
	}

	enabled := 0
	if stats.Mem.MemAvailableEnabled == true {
		enabled = 1
	}
	_, errInsert = stmtMem.Exec(now, stats.Mem.Available, stats.Mem.Buffers, stats.Mem.Cached, stats.Mem.Free, stats.Mem.Inactive, enabled, stats.Mem.SwapCached, stats.Mem.SwapFree, stats.Mem.SwapTotal, stats.Mem.SwapUsed, stats.Mem.Total, stats.Mem.Used)
	if errInsert != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to insert 'mem' data. ERROR: %s", errInsert.Error()))
	}

	for _, item := range stats.Net {
		_, errInsert = stmtNet.Exec(now, item.Name, item.RxBytes, item.TxBytes)
		if errInsert != nil {
			applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to insert 'net' data. ERROR: %s", errInsert.Error()))
		}
	}

	for _, item := range stats.FS {
		_, errInsert = stmtFS.Exec(now, item.Name, item.ReadsCompleted, item.WritesCompleted)
		if errInsert != nil {
			applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to insert 'fs' data. ERROR: %s", errInsert.Error()))
		}
	}

	_, errInsert = stmtLoad.Exec(now, stats.Avg.Loadavg1, stats.Avg.Loadavg5, stats.Avg.Loadavg15)
	if errInsert != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to insert 'load' data. ERROR: %s", errInsert.Error()))
	}

}

func prepareStatements(database *sql.DB) error {

	var errStmtCPU error
	stmtCPU, errStmtCPU = database.Prepare("INSERT INTO cpu (dateandtime,count,guest,gestNice,idle,iowait,irq,nice,softIrq,statCount,steal,system,total,user) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
	if errStmtCPU != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to prepare 'cpu' insert statement. ERROR: %s", errStmtCPU.Error()))
		return errStmtCPU
	}

	var errStmtMem error
	stmtMem, errStmtMem = database.Prepare("INSERT INTO mem (dateandtime,available,buffers,cached,free,inactive,enabled,swapCached,swapFree,swapTotal,swapUsed,total,used) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)")
	if errStmtMem != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to prepare 'mem' insert statement. ERROR: %s", errStmtMem.Error()))
		return errStmtMem
	}

	var errStmtNet error
	stmtNet, errStmtNet = database.Prepare("INSERT INTO net (dateandtime,interface,rxBytes,txBytes) VALUES (?,?,?,?)")
	if errStmtNet != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to prepare 'net' insert statement. ERROR: %s", errStmtNet.Error()))
		return errStmtNet
	}

	var errStmtFS error
	stmtFS, errStmtFS = database.Prepare("INSERT INTO fs (dateandtime,disk,reads,writes) VALUES (?,?,?,?)")
	if errStmtFS != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to prepare 'fs' insert statement. ERROR: %s", errStmtFS.Error()))
		return errStmtFS
	}

	var errStmtLoad error
	stmtLoad, errStmtLoad = database.Prepare("INSERT INTO load (dateandtime,load1,load5,load15) VALUES (?,?,?,?)")
	if errStmtLoad != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to prepare 'load' insert statement. ERROR: %s", errStmtLoad.Error()))
		return errStmtLoad
	}

	return nil
}

func createTables(database *sql.DB) error {

	// TABLES
	stmt, errStmt := database.Prepare("CREATE TABLE IF NOT EXISTS cpu (id INTEGER PRIMARY KEY, dateandtime TEXT, count INTEGER, guest INTEGER, gestNice INTEGER, idle INTEGER,iowait INTEGER, irq INTEGER, nice INTEGER, softIrq INTEGER, statCount INTEGER, steal INTEGER, system INTEGER, total INTEGER, user INTEGER)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create table statement for 'cpu' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec := stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'cpu' table creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE TABLE IF NOT EXISTS mem (id INTEGER PRIMARY KEY, dateandtime TEXT, available INTEGER, buffers INTEGER, cached INTEGER, free INTEGER,inactive INTEGER, enabled INTEGER, swapCached INTEGER, swapFree INTEGER, swapTotal INTEGER, swapUsed INTEGER, total INTEGER, used INTEGER)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create table statement for 'memory' table . ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'memory' table creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE TABLE IF NOT EXISTS net (id INTEGER PRIMARY KEY, dateandtime TEXT, interface INTEGER, rxBytes INTEGER, txBytes INTEGER)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create table statement for 'net' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'net' table creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE TABLE IF NOT EXISTS fs (id INTEGER PRIMARY KEY, dateandtime TEXT, disk INTEGER, reads INTEGER, writes INTEGER)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create table statement for 'fs' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'fs' table creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE TABLE IF NOT EXISTS load (id INTEGER PRIMARY KEY, dateandtime TEXT, load1 REAL, load5 REAL, load15 REAL)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create table statement for 'load' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'load' table creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE INDEX IF NOT EXISTS idx_cpu_time ON cpu (dateandtime DESC)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create index statement for 'cpu' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	// INDEXES
	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'cpu' endex creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE INDEX IF NOT EXISTS idx_mem_time ON mem (dateandtime DESC)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create index statement for 'mem' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'mem' endex creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE INDEX IF NOT EXISTS idx_net_time ON net (dateandtime DESC)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create index statement for 'net' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'net' endex creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE INDEX IF NOT EXISTS idx_fs_time ON fs (dateandtime DESC)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create index statement for 'fs' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'fs' endex creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	stmt, errStmt = database.Prepare("CREATE INDEX IF NOT EXISTS idx_load_time ON load (dateandtime DESC)")
	if errStmt != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Failed to create index statement for 'load' table. ERROR: %s", errStmt.Error()))
		return errStmt
	}

	_, errExec = stmt.Exec()
	if errExec != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ServerStats", fmt.Sprintf("Error executing 'load' endex creating statement. ERROR: %s", errExec.Error()))
		return errExec
	}

	return nil
}
