package applog

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/joaorsilva/alpissa/utils"
)

// AppLog - Structure
type AppLog struct {
	Module string
	Path   string
}

// AppLogs - Map of application logs
var AppLogs = make(map[string]*AppLog)

// New - Creates a new App Log
func New(module string, dir string) error {

	t := time.Now()

	if utils.DirExists(dir) == false {
		return errors.New("log directory doesn't exists")
	}

	path := fmt.Sprintf("%s/%s_%04d%02d%02d.log", dir, module, t.Year(), t.Month(), t.Day())

	applog := &AppLog{Module: module, Path: path}

	AppLogs[module] = applog

	return nil
}

// Log - Log a message to the log
func (applog *AppLog) Log(subsystem string, category string, message string) {

	f, err := os.OpenFile(applog.Path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Can't create log file %s %s\n", applog.Path, err.Error())
	}

	defer f.Close()

	t := time.Now()
	f.WriteString(fmt.Sprintf("[%04d-%02d-%02d %02d:%02d:%02d] [%10s] %s: %s\n", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), subsystem, category, message))
}

// Destroy - Removes an App Log entry
func (applog *AppLog) Destroy() {
	delete(AppLogs, applog.Module)
}
