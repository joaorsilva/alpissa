package ossignals

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtorunners"

	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/serverstats"
)

// Signals - Listen to OS signals
func Signals(ctx context.Context, httpServer *http.Server) {

	// Creates a chanel for the OS signals
	done := make(chan os.Signal, 1)

	applog.AppLogs["ALPISSA"].Log("INFO", "Main", "Seting up SIGXXX handler")
	// Watch for the Interrupt, SIGINT and SIGTERM OS signals
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// If one of the signals is sent to the process terminates the service
	<-done

	applog.AppLogs["ALPISSA"].Log("INFO", "Main", "Server termination request received")

	for index := range dtorunners.Data {
		//TODO: Send quit to servers
		dtorunners.Data[index].ExpectedToRun = false
	}

	// Waits 10 seconds for the servers to terminate in the case we have some
	for len(dtorunners.Data) > 0 {
		applog.AppLogs["ALPISSA"].Log("INFO", "Main", fmt.Sprintf("Waiting for %d servers to terminate.", len(dtorunners.Data)))
		time.Sleep(1 * time.Second)
	}

	serverstats.Stop()

	// Waits 5 seconds for the HTTP service to terminate
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		// TODO: Stop all runing instances
		cancel()
	}()

	// Shuts down the HTTP service
	if err := httpServer.Shutdown(ctx); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Main", fmt.Sprintf("Server termination failed %s", err.Error()))
		log.Fatalf("[ SIGNALS ] - Shutdown failed:%+v!", err)
	}

	applog.AppLogs["ALPISSA"].Log("INFO", "Main", "Server terminated successfully!")
}
