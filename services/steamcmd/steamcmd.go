package steamcmd

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtostatus"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

// File - SteamCMD file from the install manifest
type File struct {
	Path    string
	Size    uint32
	Number1 uint64
	Number2 uint64
}

const (
	// SteamCmdCommand - SteamCMD Executable
	SteamCmdCommand = "steamcmd.sh"
)

var (
	fileline File
)

// Check - Check if the steam CMD is installed
func Check() bool {
	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Cheking if the SteamCMD is installed...")

	if utils.DirExists(fmt.Sprintf("%s/%s/sandstorm-server", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath)) == false {
		MakeDirs()
		return false
	}

	steamFile := fmt.Sprintf("%s/%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, SteamCmdCommand)
	if utils.FileExists(steamFile) == true {

		if CheckFiles() != true {
			return false
		}

		dtostatus.Data.Steam.IsInstaled = true
		dtostatus.Data.Steam.IsDownloaded = true

		applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD is installed.")
		return true
	}

	dtostatus.Data.Steam.IsInstaled = false
	applog.AppLogs["ALPISSA"].Log("WARNING", "Steam", "SteamCMD is not installed.")

	return false
}

// CheckFiles - Open the Steam installed manifest and check for the files
func CheckFiles() bool {

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Checking SteamCMD files...")

	file, errFile := os.Open(fmt.Sprintf("%s/%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, "/package/steam_cmd_linux.installed"))
	if errFile != nil {
		applog.AppLogs["ALPISSA"].Log("WARNING", "Steam", "Could not find SteamCMD manifest file.")
		return false
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	var version string
	var subversion string

	for scanner.Scan() {
		line := scanner.Text()
		_, errScan := fmt.Sscanf(line, "%s,%d;%d;%d", &fileline)
		if errScan != nil {
			if strings.Contains(line, "VERSION=") {
				version = strings.Trim(line, "VERSION= ")
			}
			if strings.Contains(line, "OSVER=-") {
				subversion = strings.Trim(line, "OSVER=- ")
			}
		} else {
			if utils.FileExists(fileline.Path) == true {
				applog.AppLogs["WARNING"].Log("INFO", "Steam", fmt.Sprintf("File %s is reported installed but it was not found.", fileline.Path))
				return false
			}
		}
	}

	dtostatus.Data.Steam.Version = fmt.Sprintf("%s.%s", version, subversion)

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", fmt.Sprintf("Steam CMD is version %s.", dtostatus.Data.Steam.Version))
	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD files are OK")

	return true
}

// Download - Download Steam CMD file from the internet
func Download() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Dowloading SteamCMD...")

	dtostatus.Data.Steam.IsDownloading = true
	if errDownload := utils.DownloadFile(dtoconfig.Data.SteamCmdURL, fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath)); errDownload != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Steam", fmt.Sprintf("Error downloading SteamCMD. ERROR: %s", errDownload.Error()))
		dtostatus.Data.Steam.IsDownloading = false
		return errDownload
	}

	dtostatus.Data.Steam.IsDownloading = true
	dtostatus.Data.Steam.IsDownloaded = true
	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD downloaded")

	return nil
}

// Unpack - Unpack SteamCMD tar.gz
func Unpack() error {

	dtostatus.Data.Steam.IsInstaling = true

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Unpacking SteamCMD...")

	cmd := exec.Command("tar", "-xf", fmt.Sprintf("%s/%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, dtoconfig.Data.SteamCmdURLFile), "-C", fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath))
	if err := cmd.Run(); err != nil {
		dtostatus.Data.Steam.IsInstaling = true
		applog.AppLogs["ALPISSA"].Log("ERROR", "Steam", fmt.Sprintf("SteamCMD unpack failed. ERROR: %s", err.Error()))
		return err
	}

	cmd.Wait()

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD unpacked.")
	dtostatus.Data.Steam.IsInstaling = false

	return nil
}

// Update - Update SteamCMD
func Update() error {

	dtostatus.Data.Steam.IsInstaling = true
	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Running SteamCMD to update it")

	cmd := exec.Command(fmt.Sprintf("%s/%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath, SteamCmdCommand), "+quit")
	if err := cmd.Run(); err != nil {
		dtostatus.Data.Steam.IsInstaling = true
		applog.AppLogs["ALPISSA"].Log("ERROR", "Steam", fmt.Sprintf("Failed to run SteamCMD (check if you have the SteamCMD requirements installed in yor system). ERROR: %s", err.Error()))
		return err
	}

	cmd.Wait()

	dtostatus.Data.Steam.IsInstaling = false
	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD updated.")

	return nil
}

// Delete - Removes SteamCMD from the computer
func Delete() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Removing SteamCMD instalation.")
	errRemove := os.RemoveAll(fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath))
	if errRemove != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Steam", fmt.Sprintf("Error removing SteamCDM. ERROR: %s", errRemove.Error()))
		return errRemove
	}

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD instalation removed.")

	return MakeDirs()
}

// MakeDirs - Creates SteamCMD instalation directories
func MakeDirs() error {

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "Creating SteamCMD instalation directories.")

	errMkDir := os.MkdirAll(fmt.Sprintf("%s/%s/sandstorm-server", dtoconfig.Data.BaseDir, dtoconfig.Data.SteamPath), 0755)
	if errMkDir != nil {

		applog.AppLogs["ALPISSA"].Log("ERROR", "Steam", fmt.Sprintf("Failed to create SteamCMD instalation directories. ERROR: %s", errMkDir.Error()))
		return errMkDir

	}

	applog.AppLogs["ALPISSA"].Log("INFO", "Steam", "SteamCMD instalation directories created.")

	return nil
}
