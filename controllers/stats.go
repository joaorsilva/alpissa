package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtostats"
	"github.com/gin-gonic/gin"
)

// StatsCPU - Receives a list GET request
func StatsCPU(c *gin.Context) {

	stats, err := dtostats.GetCPU("", "")
	if err != nil {
		c.AbortWithStatusJSON(err.Code, err)
		return
	}

	if len(*stats) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, stats)

}

// StatsMem - Receives a list GET request
func StatsMem(c *gin.Context) {

	stats, err := dtostats.GetMem("", "")
	if err != nil {
		c.AbortWithStatusJSON(err.Code, err)
		return
	}

	if len(*stats) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, stats)

}
