package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtorunners"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtostatus"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/services/sandstorm"
	"bitbucket.org/joaorsilva/alpissa/services/steamcmd"
	"github.com/gin-gonic/gin"
)

// StatusGet - Gets the server status
func StatusGet(c *gin.Context) {

	steamcmd.Check()
	sandstorm.Check()
	dtostatus.Data.NumConfigurations = len(dtoservers.Data)
	dtostatus.Data.NumRunningServers = len(dtorunners.Data)

	c.JSON(200, &dtostatus.Data)
}

// SteamInstall - If SteamCMD is already well installed the it performs an update ortherwise it performs a downlaod with full install
func SteamInstall(c *gin.Context) {

	go func() {
		// TODO: STOP all runners
		if steamcmd.Check() == true {
			steamcmd.Update()
			return
		}

		if errDownload := steamcmd.Download(); errDownload != nil {
			return
		}

		if errUnpack := steamcmd.Unpack(); errUnpack != nil {
			return
		}

		if errUpdate := steamcmd.Update(); errUpdate != nil {
			return
		}

		// TODO: START all runners

	}()

	c.Status(202)

}

// SteamUpdate - SteamCMD to update if necessary
func SteamUpdate(c *gin.Context) {

	if dtostatus.Data.Steam.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Steam", Message: "SteamCMD isn't installed so it can't be updated"})
		return
	}

	go func() {
		// TODO: STOP all runners

		if errUpdate := steamcmd.Update(); errUpdate != nil {
			return
		}

		// TODO: START all runners
	}()

	c.Status(202)
}

// SteamDelete - SteamCMD to update if necessary
func SteamDelete(c *gin.Context) {

	if dtostatus.Data.Steam.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Steam", Message: "SteamCMD isn't installed so it can't be deleted"})
		return
	}

	go func() {
		// TODO: STOP all runners

		if errDelete := steamcmd.Delete(); errDelete != nil {
			return
		}

		dtostatus.Data.Sandstorm.IsInstaled = false

	}()

	c.Status(202)

}

// SandstormInstall - Installs or updates the Sandstorm server
func SandstormInstall(c *gin.Context) {
	// TODO: STOP all runners

	go func() {
		sandstorm.Update()
		// TODO: START all runners
	}()

	c.Status(202)
}

// SandstormDelete - Deletes the Sandstorm server instalation
func SandstormDelete(c *gin.Context) {

	if dtostatus.Data.Sandstorm.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Steam", Message: "Sandstorm isn't installed so it can't be deleted"})
		return
	}

	// TODO: STOP all runners

	go func() {
		sandstorm.Remove()
	}()

	c.Status(202)
}
