package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtogamemodes"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// GameModesList - Receives a list GET request
func GameModesList(c *gin.Context) {

	if len(dtogamemodes.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtogamemodes.Data)

}

// GameModesGet - Receives a single object GET request
func GameModesGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtogamemodes.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)
}

// GameModesCreate - Receives a single object POST request
func GameModesCreate(c *gin.Context) {

	// Instantiate a new object
	var gamemode dtogamemodes.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &gamemode)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := gamemode.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": gamemode.ID})

}

// GameModesUpdate - Receives a single object PUT request
func GameModesUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var gamemode dtogamemodes.Dto

	// Set the object id with the ID received
	gamemode.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &gamemode)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := gamemode.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// GameModesDelete - Receives a single object DELETE request
func GameModesDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var gamemode dtogamemodes.Dto

	// Set the object id with the ID received
	gamemode.ID = ID

	// Delete the object
	errDelete := gamemode.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
