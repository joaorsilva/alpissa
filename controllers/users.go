package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtousers"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// UsersList - Receives a list GET request
func UsersList(c *gin.Context) {

	if len(dtousers.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	var ret dtousers.Dtos
	for _, user := range dtousers.Data {
		user.Password = ""
		ret = append(ret, user)
	}

	c.JSON(200, dtousers.Data)

}

// UsersGet - Receives a single object GET request
func UsersGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtousers.Data {
		if item.ID == ID {
			item.Password = ""
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// UsersCreate - Receives a single object POST request
func UsersCreate(c *gin.Context) {

	// Instantiate a new object
	var user dtousers.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &user)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := user.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": user.ID})

}

// UsersUpdate - Receives a single object PUT request
func UsersUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var user dtousers.Dto

	// Set the object id with the ID received
	user.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &user)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := user.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// UsersDelete - Receives a single object DELETE request
func UsersDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var user dtousers.Dto

	// Set the object id with the ID received
	user.ID = ID

	// Delete the object
	errDelete := user.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
