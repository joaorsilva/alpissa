package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtomutators"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// MutatorsList - Receives a list GET request
func MutatorsList(c *gin.Context) {

	if len(dtomutators.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtomutators.Data)

}

// MutatorsGet - Receives a single object GET request
func MutatorsGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtomutators.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// MutatorsCreate - Receives a single object POST request
func MutatorsCreate(c *gin.Context) {

	// Instantiate a new object
	var mutator dtomutators.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &mutator)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := mutator.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": mutator.ID})

}

// MutatorsUpdate - Receives a single object PUT request
func MutatorsUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var mutator dtomutators.Dto

	// Set the object id with the ID received
	mutator.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &mutator)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := mutator.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// MutatorsDelete - Receives a single object DELETE request
func MutatorsDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var mutator dtomutators.Dto

	// Set the object id with the ID received
	mutator.ID = ID

	// Delete the object
	errDelete := mutator.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
