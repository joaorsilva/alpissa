package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtosides"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// SidesList - Receives a list GET request
func SidesList(c *gin.Context) {

	if len(dtosides.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtosides.Data)

}

// SidesGet - Receives a single object GET request
func SidesGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtosides.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// SidesCreate - Receives a single object POST request
func SidesCreate(c *gin.Context) {

	// Instantiate a new object
	var side dtosides.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &side)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := side.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": side.ID})

}

// SidesUpdate - Receives a single object PUT request
func SidesUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var side dtosides.Dto

	// Set the object id with the ID received
	side.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &side)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := side.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// SidesDelete - Receives a single object DELETE request
func SidesDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var side dtosides.Dto

	// Set the object id with the ID received
	side.ID = ID

	// Delete the object
	errDelete := side.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
