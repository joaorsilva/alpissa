package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtolighting"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// LightingList - Receives a list GET request
func LightingList(c *gin.Context) {

	if len(dtolighting.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtolighting.Data)

}

// LightingGet - Receives a single object GET request
func LightingGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtolighting.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// LightingCreate - Receives a single object POST request
func LightingCreate(c *gin.Context) {

	// Instantiate a new object
	var lighting dtolighting.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &lighting)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := lighting.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": lighting.ID})

}

// LightingUpdate - Receives a single object PUT request
func LightingUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var lighting dtolighting.Dto

	// Set the object id with the ID received
	lighting.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &lighting)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := lighting.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// LightingDelete - Receives a single object DELETE request
func LightingDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var lighting dtolighting.Dto

	// Set the object id with the ID received
	lighting.ID = ID

	// Delete the object
	errDelete := lighting.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
