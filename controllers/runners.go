package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtorunners"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtostatus"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httperrors"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
)

// RunnersList - Receives a list GET request
func RunnersList(c *gin.Context) {

	// Check if we have any players
	if len(dtorunners.Data) == 0 {
		// If not return not found error
		c.AbortWithStatus(404)
		return
	}

	var runners dtorunners.Dtos
	for _, item := range dtorunners.Data {
		var newRunner dtorunners.Dto
		copier.Copy(&newRunner, item)
		newRunner.Server.Files = nil
		runners = append(runners, &newRunner)
	}
	// If we have return the entire list
	c.JSON(200, runners)

}

// RunnersGet - Receives a single object GET request
func RunnersGet(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Search for the object
	for _, item := range dtorunners.Data {
		if item.Server.ID == ID {
			// If found return it
			var newRunner dtorunners.Dto
			copier.Copy(&newRunner, item)
			newRunner.Server.Files = nil
			c.JSON(200, newRunner)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)
}

// RunnersStart - Receives a request to start a runner
func RunnersStart(c *gin.Context) {

	if dtostatus.Data.Steam.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Runners", Message: "SteamCMD isn't installed so we can't start a server."})
		return
	}

	if dtostatus.Data.Sandstorm.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Runners", Message: "Sandstorm server isn't installed so we can't start a server."})
		return
	}

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Search for the object
	for index, item := range dtoservers.Data {
		if item.ID == ID {

			runner := dtorunners.NewRunner(dtoservers.Data[index])
			runner.Start()

			// If found return it
			c.Status(202)
			return
		}
	}
	c.AbortWithStatus(404)
}

// RunnersStop - Receives a request to stop a runner
func RunnersStop(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Search for the object
	for index, item := range dtorunners.Data {
		if item.Server.ID == ID {

			dtorunners.Data[index].Stop()
			// If found return it
			c.Status(202)
			return
		}
	}
	c.AbortWithStatus(404)
}

// RunnersRestart - Receives a request to restart a runner
func RunnersRestart(c *gin.Context) {

	if dtostatus.Data.Steam.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Runners", Message: "SteamCMD isn't installed so we can't start a server."})
		return
	}

	if dtostatus.Data.Sandstorm.IsInstaled == false {
		c.JSON(412, &httperrors.HTTPError{Code: 412, Context: "Runners", Message: "Sandstorm server isn't installed so we can't start a server."})
		return
	}

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Search for the object
	for index, item := range dtorunners.Data {
		if item.Server.ID == ID {

			dtorunners.Data[index].Restart()
			// If found return it
			c.Status(202)
			return
		}
	}
	c.AbortWithStatus(404)

}
