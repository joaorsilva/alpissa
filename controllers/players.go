package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoplayers"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// PlayersList - Receives a list GET request
func PlayersList(c *gin.Context) {

	// Check if we have any players
	if len(dtoplayers.Data) == 0 {
		// If not return not found error
		c.AbortWithStatus(404)
		return
	}

	// If we have return the entire list
	c.JSON(200, dtoplayers.Data)

}

// PlayersGet - Receives a single object GET request
func PlayersGet(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Search for the object
	for _, item := range dtoplayers.Data {
		if item.ID == ID {
			// If found return it
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)
}

// PlayersCreate - Receives a single object POST request
func PlayersCreate(c *gin.Context) {

	// Instantiate a new object
	var player dtoplayers.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &player)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := player.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": player.ID})
}

// PlayersUpdate - Receives a single object PUT request
func PlayersUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var player dtoplayers.Dto

	// Set the object id with the ID received
	player.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &player)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := player.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)

}

// PlayersDelete - Receives a single object DELETE request
func PlayersDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var player dtoplayers.Dto

	// Set the object id with the ID received
	player.ID = ID

	// Delete the object
	errDelete := player.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
