package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoscenariomodes"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// ScenarioModesList - Receives a list GET request
func ScenarioModesList(c *gin.Context) {

	if len(dtoscenariomodes.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtoscenariomodes.Data)

}

// ScenarioModesGet - Receives a single object GET request
func ScenarioModesGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtoscenariomodes.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// ScenarioModesCreate - Receives a single object POST request
func ScenarioModesCreate(c *gin.Context) {

	// Instantiate a new object
	var scenariomode dtoscenariomodes.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &scenariomode)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := scenariomode.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": scenariomode.ID})

}

// ScenarioModesUpdate - Receives a single object PUT request
func ScenarioModesUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var scenariomode dtoscenariomodes.Dto

	// Set the object id with the ID received
	scenariomode.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &scenariomode)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := scenariomode.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// ScenarioModesDelete - Receives a single object DELETE request
func ScenarioModesDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var scenariomode dtoscenariomodes.Dto

	// Set the object id with the ID received
	scenariomode.ID = ID

	// Delete the object
	errDelete := scenariomode.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
