package controllers

import (
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi/httphelpers"
	"github.com/gin-gonic/gin"
)

// ServersList - Receives a list GET request
func ServersList(c *gin.Context) {

	if len(dtoservers.Data) == 0 {
		c.AbortWithStatusJSON(404, nil)
		return
	}

	c.JSON(200, dtoservers.Data)

}

// ServersGet - Receives a single object GET request
func ServersGet(c *gin.Context) {

	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	for _, item := range dtoservers.Data {
		if item.ID == ID {
			c.JSON(200, item)
			return
		}
	}

	// If not found return the error
	c.AbortWithStatus(404)

}

// ServersCreate - Receives a single object POST request
func ServersCreate(c *gin.Context) {

	// Instantiate a new object
	var server dtoservers.Dto

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &server)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the new object
	errSave := server.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return the new object ID
	c.JSON(201, map[string]int{"id": server.ID})

}

// ServersUpdate - Receives a single object PUT request
func ServersUpdate(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var server dtoservers.Dto

	// Set the object id with the ID received
	server.ID = ID

	// Get the received JSON into the object
	errBody := httphelpers.GetJSONBody(c, &server)
	if errBody != nil {
		c.AbortWithStatusJSON(errBody.Code, errBody)
		return
	}

	// Save the object
	errSave := server.Save()
	if errSave != nil {
		c.AbortWithStatusJSON(errSave.Code, errSave)
		return
	}

	// Return Ok
	c.Status(200)
}

// ServersDelete - Receives a single object DELETE request
func ServersDelete(c *gin.Context) {

	// Get the ID path parameter
	ID, errID := httphelpers.GetID(c)
	if errID != nil {
		c.AbortWithStatusJSON(errID.Code, errID)
		return
	}

	// Instantiate a new object
	var server dtoservers.Dto

	// Set the object id with the ID received
	server.ID = ID

	// Delete the object
	errDelete := server.Delete()
	if errDelete != nil {
		c.AbortWithStatusJSON(errDelete.Code, errDelete)
		return
	}

	// Return Ok
	c.Status(200)
}
