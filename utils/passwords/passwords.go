package passwords

import "golang.org/x/crypto/bcrypt"

// HashString - Hashes a string
func HashString(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckHashString - Compare the string and the hash to check if they are equal
func CheckHashString(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
