package utils

import (
	"log"
	"path/filepath"
	"runtime"
)

// GetBasePath - Gets the executable base path
func GetBasePath() string {
	_, file, _, _ := runtime.Caller(1)

	basePath, errBase := filepath.Abs(filepath.Dir(file))
	if errBase != nil {
		log.Fatalf("ERROR: failed to get base path %s", errBase.Error())
	}

	return basePath
}
