package utils

import "os"

// FileExists - Checks if a file exists
func FileExists(path string) bool {
	stat, errStat := os.Stat(path)
	if os.IsNotExist(errStat) {
		return false
	}
	return !stat.IsDir()
}

// DirExists - Checks if a directory exists
func DirExists(path string) bool {
	stat, errStat := os.Stat(path)
	if os.IsNotExist(errStat) {
		return false
	}
	return stat.IsDir()
}
