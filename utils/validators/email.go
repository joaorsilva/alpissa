package validators

import (
	"log"
	"net"
	"regexp"
	"strings"
)

// Email type
type Email string

//NewEmail - Creates a new Email instance cleared
func NewEmail(s string) Email {
	return Email(s)
}

// IsValid - Returns true if the string is a valid email address
func (email *Email) IsValid() bool {

	emailAddress := string(*email)

	emailAddress = strings.TrimSpace(emailAddress)

	if len(emailAddress) < 3 && len(emailAddress) > 254 {
		return false
	}

	reg := regexp.MustCompile("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*")

	log.Println(emailAddress)
	if reg.MatchString(emailAddress) == false {
		return false
	}

	parts := strings.Split(emailAddress, "@")

	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) == 0 {
		return false
	}

	return true
}
