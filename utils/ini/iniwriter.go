package ini

import (
	"fmt"
	"os"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
)

// Writer - Writes an INI file based on the server configuration
func Writer(data *[]dtoservers.INIFileDto, path string) error {

	f, errFile := os.Create(path)
	if errFile != nil {
		return errFile
	}

	defer f.Close()

	for _, section := range *data {
		f.WriteString(fmt.Sprintf("%s\n", section.Section))
		for _, item := range section.Variables {
			switch item.Type {
			case "int":
				f.WriteString(fmt.Sprintf("%s=%d\n", item.Name, int(item.Value.(float64))))
				break
			case "bool":
				f.WriteString(fmt.Sprintf("%s=%t\n", item.Name, item.Value.(bool)))
				break
			case "float":
				f.WriteString(fmt.Sprintf("%s=%f\n", item.Name, item.Value.(float64)))
				break
			default:
				f.WriteString(fmt.Sprintf("%s=%s\n", item.Name, item.Value))
				break
			}
		}
		f.WriteString("\n")
	}

	return nil
}
