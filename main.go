package main

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/joaorsilva/alpissa/services/sandstorm"
	"bitbucket.org/joaorsilva/alpissa/services/serverstats"
	"bitbucket.org/joaorsilva/alpissa/services/steamcmd"

	"bitbucket.org/joaorsilva/alpissa/dtos/dtoconfig"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtogamemodes"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtolighting"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtomutators"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoplayers"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoscenariomodes"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtoservers"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtosides"
	"bitbucket.org/joaorsilva/alpissa/dtos/dtousers"
	"bitbucket.org/joaorsilva/alpissa/services/applog"
	"bitbucket.org/joaorsilva/alpissa/services/httpapi"
	"bitbucket.org/joaorsilva/alpissa/services/ossignals.go"
	"bitbucket.org/joaorsilva/alpissa/utils"
)

func main() {

	log.Println("Starting ALP - Internet Sandstorm Server Admin")

	// Gets the base path
	basePath := utils.GetBasePath()

	if err := dtoconfig.Load(basePath); err != nil {
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	errLog := applog.New("ALPISSA", fmt.Sprintf("%s/%s", dtoconfig.Data.BaseDir, dtoconfig.Data.LogPath))
	if errLog != nil {
		log.Fatalf("ERROR: %s\n", errLog.Error())
	}

	applog.AppLogs["ALPISSA"].Log("INFO", "Main", "Starting ALP - Internet Sandstorm Server Admin")

	serverstats.Start()

	if err := dtogamemodes.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "GameModes", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtolighting.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Lighting", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtomutators.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Mutators", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtoplayers.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Players", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtoscenariomodes.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "ScenarioModes", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtoservers.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("INFO", "Servers", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtosides.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Sides", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	if err := dtousers.Load(); err != nil {
		applog.AppLogs["ALPISSA"].Log("ERROR", "Users", err.Error())
		log.Fatalf("ERROR: %s\n", err.Error())
	}

	steamcmd.Check()
	sandstorm.Check()

	router := httpapi.NewHTTP()
	httpapi.SetRoutes(router)
	apiServer := httpapi.Listen(router)
	ctx := context.Background()
	ossignals.Signals(ctx, apiServer)

	applog.AppLogs["ALPISSA"].Log("INFO", "Main", "Terminating server")
}
